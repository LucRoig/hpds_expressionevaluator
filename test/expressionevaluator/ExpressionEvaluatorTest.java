package expressionevaluator;

import operator.Addition;
import evaluator.Constant;
import junit.framework.Assert;
import org.junit.Test;

public class ExpressionEvaluatorTest {
    
    @Test
    public void singleNumber(){
        Assert.assertEquals(1, new Constant(1).evaluate());
    }
    
    @Test
    public void anotherSingleNumber(){
        Assert.assertEquals(2.0, new Constant(2.0).evaluate());
    }
    
    @Test
    public void doubleDoubleAddition(){
        Assert.assertEquals(6.0, new Addition(new Constant(3.0), new Constant(3.0)).evaluate());
    }
    
    @Test
    public void doubleIntegerAddition(){
        Assert.assertEquals(6.5, new Addition(new Constant(3.5), new Constant(3)).evaluate());
    }
    
    @Test
    public void integerDoubleAddition(){
        Assert.assertEquals(5.5, new Addition(new Constant(2), new Constant(3.5)).evaluate());
    }
    
    @Test
    public void integerIntegerAddition(){
        Assert.assertEquals(3, new Addition(new Constant(1), new Constant(2)).evaluate());
    }
}