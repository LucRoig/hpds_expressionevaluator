package parser;

import evaluator.Constant;
import evaluator.Expression;
import java.util.Stack;
import operator.Addition;
import operator.Multiplication;

public class Parser {

    private final Stack<Token.Symbol> symbolStack;
    private final Stack<Expression> expressionStack;
    private final ExpressionFactory factory;

    public Parser(ExpressionFactory factory) {
        this.symbolStack = new Stack<>();
        this.expressionStack = new Stack<>();
        this.factory = factory;
    }

    public Expression parse(Token[] tokens) {
        for (Token token : tokens) {
            parse(token);
        }
        while (!symbolStack.isEmpty()) {
            process(symbolStack.pop());
        }
        return expressionStack.pop();
    }

    private void parse(Token token) {
        factory.build(token);
        if (token instanceof Token.Constant) {
            expressionStack.push(new Constant(getValue((Token.Constant) token)));            
        } else if (token instanceof Token.Symbol) {
            if(symbolHasEqualPrecedence((Token.Symbol)token)) process(symbolStack.pop());
            symbolStack.push((Token.Symbol)token);             
        }        
    }
    
    private void process(Token.Symbol symbol) {
        Expression right = expressionStack.pop();
        Expression left = expressionStack.pop();
        if (symbol.getToken().equals("+")) {            
            expressionStack.push(new Addition(left, right));            
        } else if (symbol.getToken().equals("*")) {            
            expressionStack.push(new Multiplication(left, right));            
        }
    }

    private Object getValue(Token.Constant constant) {
        return constant.value();
    }
    
    private boolean symbolHasEqualPrecedence(Token.Symbol symbol) {
        if (symbolStack.isEmpty()) return false;
        return (symbolStack.peek().getToken().equals("+") && symbol.getToken().equals("+") || symbolStack.peek().getToken().equals("-") && symbol.getToken().equals("+") || symbolStack.peek().getToken().equals("+") && symbol.getToken().equals("-") || symbolStack.peek().getToken().equals("-") && symbol.getToken().equals("-"));
    }



}