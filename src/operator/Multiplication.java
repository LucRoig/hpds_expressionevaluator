package operator;

import evaluator.BinaryOperation;
import evaluator.BinaryOperatorFactory;
import evaluator.Expression;

public class Multiplication<Type> extends BinaryOperation {

    public Multiplication(Expression left, Expression right) {
        super(left, right);
    }
    
    @Override
    public Object evaluate() {
        BinaryOperatorFactory binaryOperatorFactory = new BinaryOperatorFactory();
        return binaryOperatorFactory.createOperator("Mul",left.evaluate(),right.evaluate()).calculate(left.evaluate(),right.evaluate());
    }
}
