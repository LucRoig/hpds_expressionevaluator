package operator;

import evaluator.BinaryOperation;
import evaluator.BinaryOperatorFactory;
import evaluator.Expression;

public class Addition<Type> extends BinaryOperation {

    public Addition(Expression left, Expression right) {
        super(left, right);
    }
    
    @Override
    public Object evaluate() {
        BinaryOperatorFactory binaryOperatorFactory = new BinaryOperatorFactory();
        return binaryOperatorFactory.createOperator("Add",left.evaluate(),right.evaluate()).calculate(left.evaluate(),right.evaluate());
    }
}
