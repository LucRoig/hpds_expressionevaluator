package operator;

import evaluator.BinaryOperation;
import evaluator.BinaryOperatorFactory;
import evaluator.Expression;

public class Substraction extends BinaryOperation {

    public Substraction(Expression left, Expression right) {
        super(left, right);
    }
    
    @Override
    public Object evaluate() {
        BinaryOperatorFactory binaryOperatorFactory = new BinaryOperatorFactory();
        return binaryOperatorFactory.createOperator("Sub",left.evaluate(),right.evaluate()).calculate(left.evaluate(),right.evaluate());
    }
    
}
