package operator.mul;

import evaluator.BinaryOperator;


public class IntegerDoubleMul extends BinaryOperator {    
    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left * (Double) right;
    }
}

