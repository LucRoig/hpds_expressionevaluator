package operator.mul;

import evaluator.BinaryOperator;

public class DoubleIntegerMul extends BinaryOperator {
    
    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left * (Integer) right;
    }
}
