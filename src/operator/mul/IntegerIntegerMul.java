package operator.mul;

import evaluator.BinaryOperator;

public class IntegerIntegerMul extends BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left * (Integer) right;
    }
}
