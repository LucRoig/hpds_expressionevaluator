
package operator.mul;

import evaluator.BinaryOperator;

public class DoubleDoubleMul extends BinaryOperator {
    
    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left * (Double) right;
    }
}
