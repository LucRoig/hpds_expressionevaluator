package operator.add;

import evaluator.BinaryOperator;

public class DoubleIntegerAdd extends BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left + (Integer) right;
    }
}