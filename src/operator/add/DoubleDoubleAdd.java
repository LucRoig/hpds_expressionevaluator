package operator.add;

import evaluator.BinaryOperator;

public class DoubleDoubleAdd extends BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Double) left + (Double) right;
    }
}