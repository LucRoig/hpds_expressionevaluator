
package operator.add;

import evaluator.BinaryOperator;

public class IntegerIntegerAdd extends BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left + (Integer) right;
    }
}
