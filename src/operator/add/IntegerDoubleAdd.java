package operator.add;

import evaluator.BinaryOperator;

public class IntegerDoubleAdd extends BinaryOperator{

    @Override
    public Object calculate(Object left, Object right) {
        return (Integer) left + (Double) right;
    }
}