
package evaluator;

public abstract class BinaryOperator<Type> {

    public abstract Type calculate(Object left, Object right);

}
