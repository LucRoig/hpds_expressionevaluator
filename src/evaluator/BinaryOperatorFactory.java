package evaluator;

public class BinaryOperatorFactory {
    
    public BinaryOperatorFactory() {
    }
         
    private String getSignature(String operation, Object left, Object right){
        return left.getClass().getSimpleName()+right.getClass().getSimpleName()+operation;

    }
    
    public BinaryOperator createOperator (String operation, Object left, Object right){
        try{
            Class operatorClass = getClass(operation, left, right);
            return (BinaryOperator) operatorClass.newInstance();
        }catch(Exception e){
            return null;
        }
        
    }
        
    private Class getClass(String operation, Object left, Object right) {
        try {
            String signature = getSignature(operation, left, right);
            return Class.forName("operator."+operation.toLowerCase()+"."+signature);
        } catch (ClassNotFoundException ex) {
            return null;
        }
    }
    
}
